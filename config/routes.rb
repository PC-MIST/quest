Rails.application.routes.draw do
  root to: 'page#index'  

  
   match '/questime',       to: 'questime#index',       via: 'get'
  
  
   match '/info',       to: 'page#info',       via: 'get'
   match '/contact',    to: 'page#contact',    via: 'get'
   
   match '/blog',       to: 'page#blog',    	via: 'get'
   match '/blog1',       to: 'page#blog1',    	via: 'get'
   match '/blog2',       to: 'page#blog2',    	via: 'get'
   match '/blog3',       to: 'page#blog3',    	via: 'get'
   
   match '/mafia',      to: 'mafia#mafia', 	via: 'get'
 
   match '/quest',      to: 'quest#quest',  	via: 'get'
   match '/quest1',      to: 'quest#quest1',  	via: 'get'
   match '/quest2',      to: 'quest#quest2',  	via: 'get'
   match '/quest3',      to: 'quest#quest3',  	via: 'get'
 
   match '/deti',       to: 'deti#deti',   	via: 'get'
   match '/deti1',       to: 'deti#deti1',   	via: 'get'
   match '/deti2',       to: 'deti#deti2',   	via: 'get'
   match '/deti3',       to: 'deti#deti3',   	via: 'get'
  
end
